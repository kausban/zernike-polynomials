module Zernvis

using Plots
gr()
include("zernfun.jl")

function cart2pol(x, y)
    # cartesian to polar coordinate
    rho = sqrt.(x.^2 .+ y.^2)
    theta = atan.(y,x)
    return (rho, theta)
end

function genZ(n::Int, m::Int, pnum::Int, amplitude::Float64=1.0, norm::Bool=false)
    # return 2D array with points pnumXpnum
    x=y = range(-1, stop=1, length=pnum)
    ZZ = fill(NaN, (pnum, pnum))

    for ri=1:pnum, ci=1:pnum
        rho, theta = cart2pol(x[ri], y[ci])
        if rho > 1
            continue
        else
            returnVal = Zernfun.zf_comp(n,m,rho,theta,norm)
            if typeof(returnVal) != Bool
                ZZ[ri,ci] = amplitude * returnVal
            end
        end
    end

    return ZZ
end

function genZset(n::Int=5, pnum::Int=512, norm::Bool=false)
    # generate Z array for up to Z_n,n at resolution pnum
    ZZ_set=[]
    for ni=1:n, mi=-ni:2:ni
        push!(ZZ_set, genZ(ni,mi,pnum,1.0,norm))
    end
    return tuple(ZZ_set[1:end]...)
end

function plotZern(n::Int, m::Int;
                  pnum::Int=512, amplitude::Float64=1.0, norm::Bool=true)
    ZZ = genZ(n,m,pnum,amplitude,norm)
    plot(ZZ,st=:heatmap,
         grid=:false,showaxis=:false,clim=:auto,match_dimensions=:true,aspect_ratio=:equal,color=:magma_r)
end

function frameZern(ZZ;
                   fname::String="/tmp/output_frame.svg", fixed::Bool=true, lim=(-2,2))
    if fixed
        plot(ZZ,st=:heatmap,
             grid=:false,showaxis=:false,flip=:true,clim=lim,match_dimensions=:true,aspect_ratio=:equal,color=:magma_r)
    else
        plot(ZZ,st=:heatmap,
             grid=:false,showaxis=:false,clim=:auto,match_dimensions=:true,aspect_ratio=:equal,color=:magma_r)
    end
    savefig(fname)
end

function animateZern(ZZ::Tuple;
                     fname::String="/tmp/output_animation.gif", fixed::Bool=true, FPS::Int=5)
    # animate input tuple of Zset. takes argument of output filename and FPS as integer
    anim = @animate for zi=1:length(ZZ)
        if fixed
            plot(ZZ[zi],st=:heatmap,
                 grid=:false,showaxis=:false,clim=(-2,2),match_dimensions=:true,aspect_ratio=:equal,color=:magma)
        else
            plot(ZZ[zi],st=:heatmap,
                 grid=:false,showaxis=:false,clim=:auto,match_dimensions=:true,aspect_ratio=:equal,color=:magma)
        end
    end
    gif(anim, fname, fps = FPS)
end

function animateZernMode(n::Int=5, pnum::Int=512)
    # return tuple of Zset to animate 1 mode at a time till Z_n,n
    ZZ_set = genZset(n, pnum, true)
    ZZ = Array{Float64,2}[]

    for mode=1:length(ZZ_set), amplitude=-1:0.1:1
        push!(ZZ, amplitude .* ZZ_set[mode])
    end

    return tuple(ZZ...)
end

function animateZernVect(amplitudes::Tuple;
                         pnum::Int=512, norm::Bool=true)
    # return tuple of Zset to animate arbitrary combination modes from amplitude vector
    cycles = length(amplitudes)
    n, m = Zernfun.findIndex(length(amplitudes[1]))
    ZZ_set = genZset(n, pnum, norm)

    ZZ = Array{Float64,2}[]

    for ci=1:cycles
        # some limits to the random Zernike coefficients
        ZZw = 0 .* ZZ_set[1]
        for ai=3:length(ZZ_set)
            ZZw = ZZw .+ amplitudes[ci][ai] .* ZZ_set[ai]
        end
        push!(ZZ, ZZw)
    end

    return tuple(ZZ...)
end

end

# amplitudes = Zernfun.zf_randAmp(20,10,true)
# Zernvis.animateZern(Zernvis.animateZernVect(amplitudes,100))
# Zernvis.animateZern(Zernvis.animateZernMode(5,100))
