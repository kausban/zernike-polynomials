using MAT
include("zern_visualize.jl")

function infmatup(A::Array{Float64,2})
    # takes in 2D infmat and returns tuple of 1D array (vector)
    # each vector is the infunc of each electrode
    return tuple([A[:,c] for c in 1:size(A,2)]...)
end

function loadMat(fname::String, varname::String="Influence_mat")
    # load a recorded influence matrix from MATLAB. a
    # Takes arguments for filename and variable name.
    # assumes variable name if not given
    
    file = matopen(fname)
    infmat = read(file, varname)
    close(file)
    
    return infmatup(infmat)
end

function animateInfmat(infmat::Tuple, pnum::Int=512)
    j = length(infmat[1])
    n, m = Zernvis.Zernfun.findIndex(j-1)
    ZZ_set = Zernvis.genZset(n, pnum, false)

    ZZ = Array{Float64,2}[]

    for ei=1:length(infmat)
        ZZw = 0 .* ZZ_set[1]
        for ai = 4:j
            ZZw = ZZw .+ infmat[ei][ai] .* ZZ_set[ai-1]
        end
        push!(ZZ, ZZw)
    end

    return tuple(ZZ...)
end

function main()
    fname="20180410-InfMat_37.mat"
    infmat = load_mat(fname)
    Zernvis.animateZern(animateInfmat(infmat),fname="infmat_fps5.gif",fixed=false)
end
