module Zernfun

export zf_comp
export osaIndex
export findIndex

function osaIndex(n::Int, m::Int)::Int
    # returns OSA/ANSI standard indices ofr given n,m
    return (n*(n+2)+m)÷2
end

function findIndex(j::Int, n::Int=1, m::Int=-1)
    # finds appropriate (n,m) for given OSA/ANSI standard index j.
    # starting values can be given
    if n < m
        n+=1
        m=-n
        return findIndex(j,n,m)
    else
        if j == osaIndex(n,m)
            return (n,m)
        elseif j < osaIndex(n,m)
            println("ERROR: bad or illegal (n,m) guess or illegal values")
            return false
        else
            m+=2
            return findIndex(j,n,m)
        end
    end
end

function zf_randAmp(anum::Int=20, cycle::Int=1, noTTP::Bool=true)
    # return a tuple of arb amplitude vectors
    # anum: length of vector. cycle: num of vectors
    # noTTTP: if tip, tilt are 0
    if cycle < 1
        return ()
    else
        amplitudes = zeros(anum)
        if !noTTP
            amplitude[1:2] = rand(-0.5:0.01:0.5,3)
        end
        # anum > 2 ? amplitudes[3:end] = rand(-0.5:0.01:0.5,anum-3+1) : nothing
        # anum > 5 ? amplitudes[6:end] = rand(-0.3:0.01:0.3,anum-6+1) : nothing
        # anum > 9 ? amplitudes[10:end] = rand(-0.15:0.01:0.15,anum-10+1) : nothing
        anum > 2 ? amplitudes[3:end] = rand(-0.7:0.01:0.7,anum-3+1) : nothing
        anum > 5 ? amplitudes[6:end] = rand(-0.7:0.01:0.7,anum-6+1) : nothing
        anum > 9 ? amplitudes[10:end] = rand(-0.7:0.01:0.7,anum-10+1) : nothing
        return tuple(amplitudes, zf_randAmp(anum, cycle-1, noTTP)[:]...)
    end
end

function zf_R(n::Int, m::Int, r)
    # Radial part of the Zernike polynomial
    if size(r) == ()
        R=0
    else
        R=zeros(size(r))
    end
    
    for s = 0:(n-abs(m))÷2
        R = R + (((-1)^s * factorial(n-s) / (factorial(s) * factorial(-s+(n+abs(m))÷2) * factorial(-s+(n-abs(m))÷2))) .* r.^(n-2*s))
    end

    return R
end

function zf_ang(m::Int, theta)
    # Angular part of the Zernike polynomial
    if m>0
        ang = cos.(abs(m).*theta)
    elseif m<0
        ang = sin.(abs(m).*theta)
    else
        if size(theta) == ()
            ang = 1
        else
            ang = ones(size(theta))
        end
    end

    return ang
end

function zf_norm(n::Int, m::Int)
    # OSA standard Normalization factor
    del = m==0 ? 1 : 0 #krocecker delta del_{m0}
    N = sqrt(2*(n+1)/(1+del))
    return N
end

function check_nm(n::Int, m::Int)
    # check if n and m are valid
    # m and n are nonnegative integers with n >= m
    if (n>=abs(m) && n>=0 && abs(m)>=0)
        return true
    else
        println("WARNING: incorrect index values (n, m)")
        return false
    end
end

function check_r(r)
    # check r is between 0 and 1
    test_r = (r .<= 1) .* (r .>= 0)
    if findfirst(isequal(false), test_r) != nothing
        println("ERROR: 0<=r<=1 is required")
        return false
    else
        return true
    end
end

function zf_comp(n::Int, m::Int, r, theta, norm::Bool=false)
    # returns n,m Zernike polynomial for every r,theta coordinate
    # norm=true to normalize polynomial

    if !(check_r(r))
        return false
    end
    if !(check_nm(n,m))
        return false
    end

    # Radial polynomial is 0 if n-m is odd
    if !iseven(n-m)
        Z = zeros(size(r))
        return Z
    end

    if norm
        N = zf_norm(n, m)
    else
        N = 1
    end
    
    Z = N .* zf_R(n, m, r) .* zf_ang(m, theta)

    return Z
end

end
