import numpy as np
from numpy import sqrt as __sqrt__
from numpy import sin as __sin__
from numpy import cos as __cos__
from scipy.special import factorial as __fact__

def Z0(r,u,nflag=1):
    Z  =  1
    return Z

def Z1(r,u,nflag=2):
    Z  =  nflag*r*__sin__(u)
    return Z

def Z2(r,u,nflag=2):
    Z  =  nflag*r*__cos__(u)
    return Z

def Z3(r,u,nflag=__sqrt__(6)):
    Z  =  nflag*r**2*__sin__(2*u)
    return Z

def Z4(r,u,nflag=__sqrt__(3)):
    Z  =  nflag*(2*r**2-1)
    return Z

def Z5(r,u,nflag=__sqrt__(6)):
    Z  =  nflag*r**2*__cos__(2*u)
    return Z

def Z6(r,u,nflag=__sqrt__(8)):
    Z  =  nflag*r**3*__sin__(3*u)
    return Z

def Z7(r,u,nflag=__sqrt__(8)):
    Z  =  nflag*(3*r**2-2)*r*__sin__(u)
    return Z

def Z8(r,u,nflag=__sqrt__(8)):
    Z  =  nflag*(3*r**2-2)*r*__cos__(u)
    return Z

def Z9(r,u,nflag=__sqrt__(8)):
    Z  =  nflag*r**3*__cos__(3*u)
    return Z

def Z10(r,u,nflag=__sqrt__(10)):
    Z  =  nflag*r**4*__sin__(4*u)
    return Z

def Z11(r,u,nflag=__sqrt__(10)):
    Z  =  nflag*(4*r**2-3)*r**2*__sin__(2*u)
    return Z

def Z12(r,u,nflag=__sqrt__(5)):
    Z  =  nflag*(1-6*r**2+6*r**4)
    return Z

def Z13(r,u,nflag=__sqrt__(10)):
    Z  =  nflag*(4*r**2-3)*r**2*__cos__(2*u)
    return Z

def Z14(r,u,nflag=__sqrt__(10)):
    Z  =  nflag*r**4*__cos__(4*u)
    return Z

def Z15(r,u,nflag=__sqrt__(12)):
    Z  =  nflag*r**5*__sin__(5*u)
    return Z
        
def Z16(r,u,nflag=__sqrt__(12)):
    Z  =  nflag*(5*r**2-4)*r**3*__sin__(3*u)
    return Z

def Z17(r,u,nflag=__sqrt__(12)):
    Z  =  nflag*(10*r**4-12*r**2+3)*r*__sin__(u)
    return Z

def Z18(r,u,nflag=__sqrt__(12)):
    Z  =  nflag*(10*r**4-12*r**2+3)*r*__cos__(u)
    return Z

def Z19(r,u,nflag=__sqrt__(12)):
    Z  =  nflag*(5*r**2-4)*r**3*__cos__(3*u)
    return Z
    
def Z20(r,u,nflag=__sqrt__(12)):
    Z  =  nflag*r**5*__cos__(5*u)
    return Z

def Z21(r,u,nflag=__sqrt__(14)):
    Z  =  nflag*r**6*__sin__(6*u)
    return Z

def Z22(r,u,nflag=__sqrt__(14)):
    Z  =  nflag*(6*r**2-5)*r**4*__sin__(4*u)
    return Z

def Z23(r,u,nflag=__sqrt__(14)):
    Z  =  nflag*(15*r**4-20*r**2+6)*r**2*__sin__(2*u)
    return Z

def Z24(r,u,nflag=__sqrt__(7)):
    Z  =  nflag*(20*r**6-30*r**4+12*r**2-1)
    return Z

def Z25(r,u,nflag=__sqrt__(14)):
    Z  =  nflag*(15*r**4-20*r**2+6)*r**2*__cos__(2*u)
    return Z

def Z26(r,u,nflag=__sqrt__(14)):
    Z  =  nflag*(6*r**2-5)*r**4*__cos__(4*u)
    return Z

def Z27(r,u,nflag=__sqrt__(14)):
    Z =  nflag*r**6*__cos__(6*u)
    return Z

def Z28(r,u,nflag=4):
    Z =  nflag*r**7*__sin__(7*u)
    return Z

def Z29(r,u,nflag=4):
    Z =  nflag*(7*r**2-6)*r**5*__sin__(5*u)
    return Z

def Z30(r,u,nflag=4):
    Z =  nflag*(21*r**4-30*r**2+10)*r**3*__sin__(3*u)
    return Z

def Z31(r,u,nflag=4):
    Z =  nflag*(35*r**6-60*r**4+30*r**2-4)*r*__sin__(u)
    return Z

def Z32(r,u,nflag=4):
    Z =  nflag*(35*r**6-60*r**4+30*r**2-4)*r*__cos__(u)
    return Z

def Z33(r,u,nflag=4):
    Z =  nflag*(21*r**4-30*r**2+10)*r**3*__cos__(3*u)
    return Z

def Z34(r,u,nflag=4):
    Z =  nflag*(7*r**2-6)*r**5*__cos__(5*u)
    return Z

def Z35(r,u,nflag=4):
    Z =  nflag*r**7*__cos__(7*u)
    return Z

def Z40(r,u,nflag=3):
    Z = nflag*(70*r**8-140*r**6+90*r**4-20*r**2+1)
    return Z

ZZ = [Z0, Z1, Z2, Z3, Z4, Z5, Z6, Z7, Z8, Z9, Z10, Z11, Z12, Z13, Z14, Z15, Z16, Z17, Z18, Z19, Z20, Z21, Z22, Z23, Z24, Z25, Z26, Z27, Z28, Z29, Z30, Z31, Z32, Z33, Z34, Z35]


def norm_comp(n,m):
    # to implement noll normalization 
    return 1;

def zf_comp(n,m,r,theta,nflag=False):
    # check if n and m are valid
    # m and n are nonnegative integers with n >= m
    if not(n>=abs(m) and n>=0 and abs(m)>=0 and int(n)==n and int(m)==m):
        return -1

    if m>0:
        ang = __cos__(m*theta)
    elif m<0:
        ang = __sin__(m*theta)
    else:
        ang = 1

    m = abs(m)
    Z = np.zeros(r.size)

    # Radial polynomial is 0 for n-m odd
    if bool((n-m)%2):
        return Z

    for k in range(0, int((n-m)/2) + 1):
        Z = Z + (((-1)**k * __fact__((n-k))) / (__fact__(k) * __fact__(-k+(n+m)/2) * __fact__(-k+(n-m)/2))) * r**(n-2*k)

    if nflag:
        norm = norm_comp(n,m)
    else:
        norm = 1
        
    return Z*ang*norm

def zf(n,m,r,theta,nflag=False):
    # generate zernike polynomial value for all zernike mode n,m 
    # nflag=true to normalize
    j = int((n*(n+2)+m)/2)
    if j <= 36:
        if nflag == False:
            Z = ZZ[j](r,theta,1)
        else:
            Z = ZZ[j](r,theta)
    else:
        Z = zf_comp(n,m,r,theta,nflag)

    return Z
 
def zf_all(r,theta,j=36,nflag=False):
    # generate zernike polynomial value for all zernike up to j.
    # nflag=true to normalize
    if j<=36:
        Z = np.zeros((r.shape[0],j))
        if nflag == False:
            for i in range(j):
                Z[:,i] = ZZ[i](r,theta,1)

        else:
            for i in range(j):
                Z[:,i] = ZZ[i](r,theta)
    else:
        Z = np.zeros((r.shape[0],j))
        n=0
        m=0
        i=0
        while j > i:
            Z[:,i] = zf_comp(n,m,r,theta,nflag)
            i = i+1
            if n==m:
                n=n+1
                m=-n
            else:
                m=m+2
            
    return Z
