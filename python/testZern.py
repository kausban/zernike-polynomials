#!/usr/bin/env python

import numpy as np
import zernfun


def mlab_trisurf(A, B, KK):
    # trisurf using mayavi
    from scipy.spatial import Delaunay
    from mayavi import mlab
    p2d = np.vstack([A, B]).T
    d2d = Delaunay(p2d)

    fig = mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0.5, 0.5, 0.5))
    tmesh = mlab.triangular_mesh(A, B, KK, d2d.vertices,
                                 scalars=KK, colormap='coolwarm')

    mlab.colorbar()
    mlab.outline(extent=(0, 1, 0, 1, 0, 1))
    mlab.show()


def plt_trisurf(A, B, KK):
    # trisurf plot using matplotlib
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator, FormatStrFormatter

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    tri = ax.plot_trisurf(A, B, KK, cmap=cm.coolwarm,
                          linewidth=0.0, antialiased=False)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    fig.colorbar(tri, shrink=0.5, aspect=5)
    plt.show()


def fitZern(data, j=45):
    # fit displacement data to zernike polynomials up to the jth order
    n = data.shape[0]
    radius = int(n/2)

    if n%2 == 0:
        xyrange = np.arange(-radius, radius, 1)
        X,Y = np.meshgrid(xyrange, xyrange)
    else:
        xyrange = np.arange(-radius, radius+1, 1)
        X,Y = np.meshgrid(xyrange, xyrange)
        
    def cart2pol(x, y):
        rho = np.sqrt(x**2 + y**2)
        phi = np.arctan2(y, x)
        return(rho, phi)

    rho_pix, theta_pix = cart2pol(X, Y)
    is_in_circle = rho_pix <= radius
    rho = rho_pix[is_in_circle] / radius
    theta = theta_pix[is_in_circle]

    zern_val = zernfun.zf_all(rho, theta, j, False)  # generate zernicke polynomial values at each pair of r,theta for each n,m index
    ZernikeCoeff = np.matmul(np.linalg.pinv(zern_val), data[is_in_circle])  # fit data to Zernicke to estimate coefficient of data

    return ZernikeCoeff

def dispZern(zerns, flag=0):
    # disp is 2d matrix of pupil defined by zernike coefficients in vector zerns
    # flag = 1 to set tip, tilt and piston to 0
    Z = np.zeros((250, 250))
    n = Z.shape[0]

    radius = n/2
    xyrange = np.arange(-radius, radius, 1)
    X,Y = np.meshgrid(xyrange, xyrange)

    def cart2pol(x, y):
        rho = np.sqrt(x**2 + y**2)
        phi = np.arctan2(y, x)
        return(rho, phi)

    rho_pix, theta_pix = cart2pol(X, Y)
    is_in_circle = rho_pix <= radius
    rho = rho_pix[is_in_circle] / radius
    theta = theta_pix[is_in_circle]

    zern_val = zernfun.zf_all(rho, theta, 45, False)  # generate zernicke polynomial values at each pair of r,theta for each n,m index

    if flag == "1":
        zerns[0] = 0
        zerns[1] = 0
        zerns[2] = 0  # zero piston, tip, tilt

    disp = np.matmul(zern_val, zerns)

    A = X[is_in_circle]
    B = Y[is_in_circle]
    rms = np.sqrt(np.mean(np.square(disp)))
    pv = np.abs(np.max(disp) - np.min(disp))
    return A, B, disp, rms, pv


def testZern(N, M, Amp):
    # show zernike polynomial for mode N,M with amplitude Amp
    Z = np.zeros((250, 250))

    n = Z.shape[0]

    radius = n/2
    xyrange = np.arange(-radius, radius, 1)
    X, Y = np.meshgrid(xyrange, xyrange)

    def cart2pol(x, y):
        rho = np.sqrt(x**2 + y**2)
        phi = np.arctan2(y, x)
        return(rho, phi)

    rho_pix, theta_pix = cart2pol(X, Y)
    is_in_circle = rho_pix <= radius
    rho = rho_pix[is_in_circle] / radius
    theta = theta_pix[is_in_circle]

    norm = False
    # KK = zernfun.zf_all(rho,theta,36,norm)
    KK2 = Amp*zernfun.zf(N, M, rho, theta, norm)

    A = X[is_in_circle]
    B = Y[is_in_circle]

    # mlab_trisurf(A,B,KK2)
    plt_trisurf(A, B, KK2)

    return A, B, KK2
