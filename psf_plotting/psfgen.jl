module psfgen

using Plots, FFTW, Images
gr()

function cart2pol(x, y)
    # cartesian to polar coordinate
    rho = sqrt.(x.^2 .+ y.^2)
    theta = atan.(y,x)
    return (rho, theta)
end

function Γ(rho::Float64)
    # simple gaussian
    return 1.0 .* exp.(-(rho.^2) ./ 1.0)
end

function focus(ZZ)
    # 2D fft. Returns Intensity
    FZZ = abs.(fftshift(fft(ZZ))).^2
    return FZZ ./ maximum(FZZ)
end

function gauWF(pnum::Int, aberration::Array{Float64,2})
    # accepts pnum:resolution and aberration array. Returns gaussian distribution with imaganary phase 
    x=y = range(-1, stop=1, length=pnum)
    ZZ = zeros(Complex{Float64}, pnum, pnum)
    
    for ri=1:pnum, ci=1:pnum
        rho, theta = cart2pol(x[ri], y[ci])
        if rho > 1
            ZZ[ri,ci] = 0.0 + 0.0im
        else
            ZZ[ri,ci] = Γ(rho) .* exp(im .* aberration[ri,ci])
        end
    end
    return ZZ
end

function focusUpscaled(ZZ::Array)
    # upscales 2D image by padding. For  higher res PSF when using focus to estimate focal plane info
    la = size(ZZ,1)
    padval = la*8
    ZZ2 = padarray(ZZ, Pad((padval-la)÷2,(padval-la)÷2))
    return focus(ZZ2)
end

function plotPSF(FZZ::Array{Float64,2}, ps::Int=256)
    # plots 2D intensity distribution. ps: defines central crop
    la = size(FZZ,1)÷2
    cropr = la-ps÷2:la+ps÷2-1
    
    plot(FZZ[cropr, cropr],st=:heatmap,
         grid=:false,showaxis=:false,clim=:auto,match_dimensions=:true,aspect_ratio=:equal)
end

function animateAbber(AA::Tuple; fname::String="/tmp/PSF_animation.gif", FPS::Int=5, ps::Int=128)
    
    pnum = size(AA[1],1)
    anim = @animate for ai=1:length(AA)
        FZZ = focusUpscaled(gauWF(pnum, AA[ai]))
        cropr = size(FZZ,1)÷2-ps÷2:size(FZZ,1)÷2+ps÷2-1
        plot(FZZ[cropr, cropr],st=:heatmap,
             grid=:false,showaxis=:false,clim=:auto,match_dimensions=:true,aspect_ratio=:equal)
    end
    gif(anim, fname, fps =FPS)
end

end #module

# amplitudes = Zernfun.zf_randAmp(20,100,true)
# amp1 = amplitudes
# amp2 = amplitudes
# kk = amplitudes[1]
# for i=1:100
#     global amp1
#     global amp2
#     global kk
#     amp2= tuple(zeros(20), amp2[:]...)
#     amp1= tuple(kk, amp1[:]...)
# end

# AA = Zernvis.animateZernVect(amp1,512)
# Zernvis.animateZern(AA, FPS=30)
# BB = Zernvis.animateZernVect(amp2,512)
# psfgen.animateAbber(BB, ps=128, FPS=15)
