module gv
using PyCall
function __init__()
    py"""
    from casadi import *
    from scipy.io import loadmat
    import pickle
    import sys
    import numpy as np

    def loadinfmat(fn):
        # fn: filename of saved influence matrix
        # returns influence matrix as 2D numpy array
        if fn.endswith('.mat'):
            I = loadmat(fn)
            I = I['Influence_mat']
            infmat = np.asmatrix(I)
        else:
            with open(fn, 'rb') as handle:
                I = pickle.load(handle)
                if type(I) is list:
                    infmat = np.transpose(np.array(I[0])) / 10**6
                else:
                    infmat = I

        return infmat

    def getVolts(infmat, zerns, fitoff=None):
        # infmat: the influnce matrix as 2d numpy array
        # zerns: target configuration as vector of Zernike coefficients
        # fitoff: some offset Zernike coefficients value
        
        I = infmat
        if fitoff is not None:
            zerns = zerns - fitoff

        # Variables
        U = MX.sym('U', I.shape[1], 1)

        # constraints function
        f = 0.5*mtimes(mtimes(transpose(U), mtimes(transpose(I), I)),
                       U) + mtimes(transpose(-1*mtimes(transpose(I), zerns)), U)

        # solver
        nlp = {'x': U, 'f': f}
        MySolver = "ipopt"
        opts = {}
        opts["ipopt.linear_solver"] = "mumps"  # other solvers ma27,mumps,ma57,ma97
        opts["ipopt.max_iter"] = 3000  
        opts["ipopt.tol"] = 1E-10

        opts["verbose_init"] = False
        opts["verbose"] = False
        opts["print_time"] = False
        opts["ipopt.print_level"] = 0
        opts["ipopt.sb"] = "yes"

        # Allocate solver
        solver = nlpsol("solver", MySolver, nlp, opts)

        # Solve
        x_lb = 0 #lower voltage bound
        x_ub = 400**2 #upper voltage bound
        res = solver(lbx=x_lb,
                     ubx=x_ub)

        V = sqrt(res['x'])

        return np.array(V)
    """
end

loadinfmat(x)=py"loadinfmat"(x)
getVolts(x,y)=py"getVolts"(x,y)
#getVolts(x,y,z)=py"getVolts"(x,y,z)

end
