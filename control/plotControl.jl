# outer ring: 4
# center electrode: 35
using NaNMath
using MAT
include("getVolts.jl")
include("/home/lamdacore/Desktop/Zernike/zernike-polynomials/julia/zern_visualize.jl")

res=256

function getSim(zerns, fname::String, res)
    ## simulated volts and WF    
    infmat = gv.loadinfmat(fname)
    volts = gv.getVolts(infmat,zerns)

    zernE = infmat * volts.^2
    simFrame = Zernvis.animateZernVect(tuple(zernE[2:45]),norm=false,pnum=res)[1]
    Zernvis.frameZern(simFrame,fname="/tmp/sim.svg", lim=(-1,1))

    # return simFrame
    return simFrame, volts, zernE
end

function getRef(zerns, res)
    ## reference WF
    refFrame = Zernvis.animateZernVect(tuple(zerns[2:45]),norm=false,pnum=res)[1]
    Zernvis.frameZern(refFrame,fname="/tmp/ref.svg", lim=(-1,1))

    return refFrame
end

## RMSE
function RMSE(simFrame,refFrame)
    RMSE = NaNMath.sqrt(NaNMath.mean((simFrame - refFrame).^2))
    return RMSE
end

function loadOffZern(fname::String, varname::String="Off_Zern_Coeffs")
    # load a recorded influence matrix from MATLAB. a
    # Takes arguments for filename and variable name.
    # assumes variable name if not given
    
    file = matopen(fname)
    fitoff = read(file, varname)
    close(file)
    
    return fitoff
end

# target
zerns = zeros(45)
#zerns[6] = -3

#fname="20180410-InfMat_37.mat"
fname="20191210-InfMat_38_1.mat"
fname_offset="20191210-OffZC_38_1.mat"
fitoff = loadOffZern(fname_offset)

# simFrame = getSim(zerns, fname, res)
simFrame, volts, zernE = getSim(zerns-fitoff, fname, res)
refFrame = getRef(zerns-fitoff, res)
print(RMSE(simFrame,refFrame))
