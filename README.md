# Zernike Polynomial generation and plotting scripts. In python and in julia.

## Julia
* zernfun.jl: generic functions/modules to generate arbitarary Zernike polynomials
* zern_visualize.jl: script to create plots and animations
* infmat.jl: handle influence matrix data files (also form matlab)
* psfgen.jl: generates a gaussian and aberrates it with Zernike Coef defined phase modulation. fft based PSF plots.

## Python
* testZern.py: Some usage and testing
* zernfun.py: Python functions to generate Zernike polynomials
* Functions expect an input of vectors r and theta (polar coordinates) and return back a vector with the values a described by zernike polynomial mode n,m.

## Control
Some functions to show how ipopt can be used to compute the required voltage for a target displacement. A complete influence matrix is used for this process. 

Uses Casadi (web.casadi.org) with python to easily interface with ipopt.
